package com.example.navigationdrawerexample;

import static android.view.View.GONE;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.navigationdrawerexample.Class.LoginRegister;
import com.google.android.material.textfield.TextInputEditText;


import java.util.HashMap;

public class ActivityLogin extends AppCompatActivity {



    TextInputEditText editTextUserName, editTextPassword;
    TextView  textViewSignUp;
    ProgressBar progressBar;
    Button buttonLogin;
    public static final String EXTRA_MESSAGE = "com.example.navigationdrawerexample.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        //SignUp
        textViewSignUp = findViewById(R.id.textSignUp);
        textViewSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), ActivitySignUp.class);
                startActivity(intent);
                finish();
            }
        });

        editTextUserName = findViewById(R.id.textInputEditUsername);
        editTextPassword =   findViewById(R.id.textInputEditPassword);
        buttonLogin = (Button) findViewById(R.id.btnLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);


       buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String userName = editTextUserName.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();

               if(verificarInput(userName,password)){

                   LoginRegister register = new LoginRegister(userName, password);

                   try {
                       register.loginUser(progressBar, getApplicationContext());


                   }catch (Exception e){}


               }


            }
        });

    }


    public boolean verificarInput(String userName, String senha){



        //Validar campos
        if(TextUtils.isEmpty(userName)){
            editTextUserName.setError("Digite o nome de usuário");
            editTextUserName.requestFocus();
             return false;
        }

        if(TextUtils.isEmpty(senha)){
            editTextPassword.setError("Digite a senha");
            editTextPassword.requestFocus();
            return false;
        }

        return true;
    }


}