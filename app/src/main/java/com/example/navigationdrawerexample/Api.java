package com.example.navigationdrawerexample;

public class Api {

    private static final String ROOT_URL = "http://192.168.1.245/LoginRegister/v1/Api.php?apicall=";

    public static final String URL_LOGIN_USER = ROOT_URL + "login";
    public static final String URL_SIGNUP_USER = ROOT_URL + "signUp";
    public static final int CODE_GET_REQUEST = 1024;
    public static final int CODE_POST_REQUEST = 1025;



}
