package com.example.navigationdrawerexample.Class;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.navigationdrawerexample.Api;
import com.example.navigationdrawerexample.PerformNetworkRequest;

import java.util.HashMap;

public class LoginRegister {

    private String fullname;
    private String email;
    private String username;
    private String password;

   public  LoginRegister(){}

    public LoginRegister( String userName, String userPassword){

        this.username = userName;
        this.password = userPassword;
    }

    public LoginRegister(String userFullname, String userEmail, String userName, String userPassword){

        this.fullname = userFullname;
        this.email = userEmail;
        this.username = userName;
        this.password = userPassword;
    }


    public void setFullname(String fullname){

        this.fullname = fullname;

    }

    public String getFullname(){

        return this.fullname;
    }


    public void  setEmail(String email){

        this.email = email;

    }

    public String getEmail(){
        return this.email;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return this.username;

    }

    public void setPassword(String password){

        this.password = password;
    }


    public void loginUser(ProgressBar progressBar, Context context) throws InterruptedException {




        HashMap<String, String> params = new HashMap<>();
        params.put("username", this.username);
        params.put("password",this.password);


        PerformNetworkRequest request = new PerformNetworkRequest(Api.URL_LOGIN_USER,
                params, Api.CODE_POST_REQUEST, progressBar, context);

        request.execute();




    }

    public void signUpRegister(ProgressBar progressBar, Context context){

        HashMap<String, String> params = new HashMap<>();
        params.put("fullname",this.fullname);
        params.put("email", this.email);
        params.put("username",this.username);
        params.put("password",this.password);

        PerformNetworkRequest request = new PerformNetworkRequest(Api.URL_SIGNUP_USER, params,
                Api.CODE_POST_REQUEST,
                progressBar, context);

        request.execute();



    }

}
