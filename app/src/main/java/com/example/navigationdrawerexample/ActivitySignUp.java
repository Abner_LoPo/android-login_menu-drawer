package com.example.navigationdrawerexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.navigationdrawerexample.Class.LoginRegister;
import com.google.android.material.textfield.TextInputEditText;

public class ActivitySignUp extends AppCompatActivity {

    TextInputEditText inputEditFullname,
                      inputEditEmail,
                      inputEditTextUsername,
                      inputEditTextPassword;
    Button btnSignUp;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        inputEditFullname = findViewById(R.id.TextInputEditFullname);
        inputEditEmail = findViewById(R.id.TextInputEditEmail);
        inputEditTextUsername = findViewById(R.id.TextInputEditUsername);
        inputEditTextPassword = findViewById(R.id.TextInputEditPassword);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        btnSignUp = (Button) findViewById(R.id.buttonSignUp);



        //Evento Click
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               String userFullname = inputEditFullname.getText().toString().trim();
               String userEmail = inputEditEmail.getText().toString().trim();
               String userName =  inputEditTextUsername.getText().toString().trim();
               String userPassword = inputEditTextPassword.getText().toString().trim();

                if(verificarInput(userFullname, userEmail, userName, userPassword)) {


                    LoginRegister register = new LoginRegister(userFullname,
                            userEmail,
                            userName,
                            userPassword);

                    register.signUpRegister(progressBar, getApplicationContext());

                    clearInputs();
                }

            }
        });


        //Voltar para tela de Login
        TextView textViewSignup = findViewById(R.id.loginText);
        textViewSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public boolean verificarInput(String nome, String email, String user, String senha){

        if(TextUtils.isEmpty(nome)){

            inputEditFullname.setError("Digite o nome completo");
            inputEditFullname.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(email)){
            inputEditEmail.setError("Digite o e-mail");
            inputEditEmail.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(user)){
            inputEditTextUsername.setError("Digite o nome de usuário");
            inputEditTextUsername.requestFocus();
            return false;
        }

        if(TextUtils.isEmpty(senha)){
           inputEditTextPassword.setError("Digite a senha");
            inputEditTextPassword.requestFocus();
            return false;
        }

        return true;
    }

    public void clearInputs(){

        inputEditFullname.setText("");
        inputEditEmail.setText("");
        inputEditTextUsername.setText("");
        inputEditTextPassword.setText("");


    }
}