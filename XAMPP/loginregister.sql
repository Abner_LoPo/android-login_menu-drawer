CREATE DATABASE loginregister;

use loginregister;


CREATE TABLE users(
id INT(11) auto_increment primary key,
fullname TEXT,
username VARCHAR(100),
password TEXT,
email VARCHAR(300));