<?php


class DbOperation{


	private $conn;



	function __construct(){

		//Getting the DbConnect.php file
        require_once dirname(__FILE__) . '/DataBase.php';
 

        $db = new DataBase();

        $this->conn = $db->dbConnect();


	}


	function prepareData($data){
		/*
			Esta função é usada para criar uma string SQL legal que você pode usar em uma instrução SQL
		*/
		return mysqli_real_scape_string($this->connect, stripslashes(htmlspecialchars($data)));

	}


	function login($table, $username, $password){

		

		$this->sql = "select * from ".$table." where username = '".$username."'";
		$result = mysqli_query($this->conn, $this->sql);//Realiza o select
		$row = mysqli_fetch_assoc($result);//Retorna um array do resultado

		if(mysqli_num_rows($result) != 0){

			$dbusername = $row['username'];
			$dbpassword = $row['password'];

			if($dbusername == $username && password_verify($password, $dbpassword)){

				$login = true;

			}else {
				$login = false;
			}
		}else{
			$login = false;
		}

		return $login;
	}



	function signUp($table, $fullname, $email, $username, $password){

		$fullname = $fullname;
		$username = $username;
		$password = password_hash($password, PASSWORD_DEFAULT);
		$email = $email;

		$this->sql = 
		"INSERT INTO ".$table." (fullname, username, password, email) VALUES ('".$fullname."','".$username."','".$password."','".$email."')";

		if(mysqli_query($this->conn,$this->sql)){

			return true;

		}else{

			return false;
		}
	}



}


?>