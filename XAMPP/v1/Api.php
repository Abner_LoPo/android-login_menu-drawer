<?php


	//getting the dboperation class
	 require_once '../includes/DbOperation.php';

	
	 
	 // função de validação de todos os parâmetros disponíveis 
	 // vamos passar os parâmetros necessários para esta função 
	 function isTheseParametersAvailable($params){

		 //assuming all parameters are available 
		 $available = true; 
		 $missingparams = ""; 
 
	 	foreach($params as $param){
	 
	 		if(!isset($_POST[$param]) || strlen($_POST[$param])<=0){

				$available = false; 
	 			$missingparams = $missingparams . ", " . $param; 
	 		}
 		}
 
		 //if parameters are missing 
		 if(!$available){

			$response = array(); 
		 	$response['error'] = true; 
			$response['message'] = 'Parameters ' . substr($missingparams, 1, strlen($missingparams)) . ' missing';
		 
			 //displaying error
			 echo json_encode($response);
	 
			 //stopping further execution
			 die();
	 	}
 	}
 
	
 	// uma matriz para exibir a resposta
 	$response = array();


  	// se for uma chamada de API
	// isso significa que um parâmetro get chamado api call é definido no URL
	// e com este parâmetro estamos concluindo que é uma chamada de API

 	if(isset($_GET['apicall'])){

 		switch($_GET['apicall']){


 			case 'login':

			
			isTheseParametersAvailable(array('username','password'));
			$db = new DbOPeration();


			$result = $db->login(
			"users",
			$_POST['username'], 
			$_POST['password']);

			if($result){

				$response['error'] = true;


				$response['message'] = "Login Success";

			}else{

				$response['error'] = false;


				$response['message'] = "Username or Password errado.";
			}
 			break;

 			case 'signUp':

 				isTheseParametersAvailable(array('fullname','email','username','password'));

 				$db = new DbOPeration();

				$result = $db->signUp(
				"users", 
				$_POST['fullname'], 
				$_POST['email'], 
				$_POST['username'], 
				$_POST['password']);

				
				if($result){
					
					$response['error'] = false;
					$response['message'] = "Sign Up Sucess";

				}else{

					$response['error'] =true;
					$response['message'] = "Sign up Failed";
				}
	
 			break;
 		}

 	}else{

 		//if it is not api call 
		 //pushing appropriate values to response array 
		 $response['error'] = true; 
		 $response['message'] = 'Invalid API Call';

 	}





 	// exibindo a resposta na estrutura json
 	echo json_encode($response);
?>